# Prototyping Project
##### Ece Erdim - Serhan Kodaman - Paola Díaz

## Geda Robot Simulator - Overview
The geda_robot_v1 package simulates the operation of a Geda robot on a construction side, which can be visualized in Rviz. It allows, with the help of a button panel, to enter a sequence of floors requests. This request is the input for the Geda robot to determinate the optimal route and move accordingly.

## Package Structure

 ```
 workspace/
      src/
         ▒   package.xml
         ▒   README.md
         ▒   setup.cfg
         ▒   setup.py
         +---geda_robot_v1
         ▒       buttonpanel.py
         ▒       collada_marker_publisher.py
         ▒       state_publisher.py
         ▒       __init__.py
         +---launch
         ▒       gedav1.launch.py
         +---meshes
         ▒       building.dae
         ▒       building.daebak
         ▒       cage3.dae
         ▒       mast4.dae
         +---resource
         ▒       geda_robot_v1
         +---rviz
         ▒       config.rviz
         +---test
         ▒       test_copyright.py
         ▒       test_flake8.py
         ▒       test_pep257.py
         +---urdf
               urdf.urdf

 ```
### Project Folders

 - **geda_robot_v1:** This folder contains Python scripts **(.py)** to create the GUI, controlling the
                     robot's movements,and publishing the marker.
 - **launch:** This folder contains the launch file **(.py)** used to iniciate the simulation in Rviz. 
               It also configures the necessary nodes and parameters for controlling the robot.
 - **meshes:** This folder contains 3D models of the robot components and marker in collada file
               format **(.dae)**. 
 - **rviz:**   This folder contains the file **(.rviz)** with the setting for visualizing the project
               in Rviz.
 - **urdf:**   This folder contains the file **(.urdf)** with the description of the physical 
               characteristics and kinematics of the robot.
      
## Used Libraries and Packages
 Required Python modules and libraries:

 - Tkinter : This library is used to create the graphical user interface.
         ```
         sudo apt install python3-tk
         python -m tkinter # to ensure that Tkinter is available now.
         ```
 - rlcpy : Used to interact with ROS2 for creating ROS nodes, publishing and subscribing topics.
 - time  : Used for introducing time delays.
 - threading : Used for creating and managing threads to handle concurrent tasks.
 - os : Used for interact with the operating system.
 - ament_index_pyhton.packages: Used for interacting with Ament package system.

 Required ROS2 packages:
 
 - sensor_msg.msg: Used to import 'JointState' message type for handling joint state information.
 - std_msgs.msg : Used to import 'Int31' message type for handeling integer data.
 - visualization_msg.msg : Used to import the 'Marker' message type.
 - robot_state_publisher: Used to publishes the state of the robot, which includes the transformation between different joints and links
 

## Installation
 In order to work with this package is necessary to have ROS 2 (Robot Operation System 2) installed.

 **NOTE:** This package was tested on ROS2 Humble.

 - ROS 2: [Installation guide](https://docs.ros.org/en/humble/Installation.html)

 ### Environment setup
   To secure the correct functioning of the package is necessary to have the following created and installed in your environment:

 - **Rviz:**  
    ```
    sudo apt install -y ros-humble.rviz2
    ```
 - **Python:**
    ```
    sudo apt install -y python3-pip
    ```
 - **Create workspace and 'src' folder:**
    ```
    mkdir ~/workspace/src
    ```
         
 ### Package Installation
    
 1. Clone this repository into the **'src'** folder of your workpace:

    ```
    cd ~/ros2ws/src # Navigate to your ROS2 workspace's src directory
    git clone https://git.rwth-aachen.de/prototyping1/geda_robot_v1.git
    ```
 2. Build your ROS2 workspace:

    ```
    cd ~/ros2ws # Make sure to be in the enviroment and not into src folder
    colcon build --packages-select geda_robot_v1
    ```

 ### Running the Simulation

 1. Source your workspace and launch the **'gedav1.launch.py'** file to start the Geda simulation in Rviz.
  
    ```
    source ~/install/setup.bash
    ros2 launch geda_robot_v1 gedav1.launch.py
    ```
 
  **NOTE:** With this the simulation will start, is necessary to wait for a couple a seconds in order to watch the marker in Rviz.


## Code Overview

- **buttonpanel:**  This code creates a button panel to control the robot using the *'Tkinter'* library in Python and integrated with ROS2 to create the comunication. The button panel allows users to select floors.
- **collada_marker_publisher:** The porpuse of this node (*'marker_publisher_node'*) is to publish a *'Marker'* message on the *'marker_topic'*.

- **state_publisher:** This script implements a ROS2 node that models the behavior of the Geda robot, handeling floor requests,and simulating the robot movements with acceleration and deceleration.

- **gedav1.launch:** This file sets up and launches various nodes for ROS2, including the robot state publisher, state publisher, button panel,marker publisher and Rviz for visualization. The URDF and Rviz configuration files are loaded, and launch arguments are declared for configuring the behavior of the system.

- **config.rviz:** This file defines the layour and settings for Rviz. Displaying the robot model and marker when running the package.
- **urdf:**   This URDF file defines the structure of the Geda robot, considering two linkds (*'mast'* and *'cage'*) connected by two joints (*'world_joint'* and *'mast_cage_joint'*). It includes visual representations for each link and specifies their relative positions and orientations.

- **setup:** This script is a setup.py file for the geda_robot_v1 ROS 2 package. It is used for specifying the metadata and configuration required to package, distribute, and install the ROS 2 package.



















