import rclpy
from rclpy.node import Node
import time
from sensor_msgs.msg import JointState
from std_msgs.msg import Int32
import threading

class StatePublisher(Node):

    def __init__(self):
        super().__init__('state_publisher')
        self.joint_state_publisher = self.create_publisher(JointState, 'joint_states', 10)
        self.subscription = self.create_subscription(Int32, 'floor_requests', self.floor_request_callback, 10)#Subscribing to floor_requests topic
        
        self.joint = JointState()
        self.joint.header.frame_id = 'mast'
        self.joint.name = ['mast_cage_joint']
        self.joint.position = [0.0]  # Initial position
        self.target_height = 0.0 #Initial target height

        self.mast_cage_joint = 0.0  # Initial height of the cage
        self.deceleration = 0.5 #Initial deceleration m/s^2
        self.acceleration = 0.5 #Initial acceleration m/s^2
        self.max_speed = 2.0 #Maximum allowed speed of the elevator m/s
        self.current_speed = 0.0 #Initial speed m/s
        self.moving = False #Flag to control movement
        self.update_period = 0.01  # Update period in seconds for the timer callback
        self.timer = self.create_timer(self.update_period, self.update_joint_state)
        self.distance_to_target = 0.0 #Initial distance to target height
        self.processing_timer = None
        self.timer_delay = 0.1

        self.floor_queue = []  # Queue to hold floor requests
        self.processing_floor_request = False 
        self.queue_lock = threading.Lock() #Lock on stored queue
        self.floor_heights = {1: 2.8, 2: 5.6, 3: 8.4, 4: 11.2, 5: 14.0, 6: 16.8, 7: 19.6, 8: 22.4, 9: 25.2} #floor heights of the marker

    
#queueing the floor_requests
    def floor_request_callback(self, msg):
        with self.queue_lock:
            self.floor_queue.append(msg.data)
        if self.processing_timer:
            self.processing_timer.cancel()
        self.processing_timer = threading.Timer(self.timer_delay, self.process_requests)
        self.processing_timer.start()



#Processing the floor requests
    def process_requests(self):
        with self.queue_lock:
            if not self.floor_queue:
                return  # Either no pending requests or already processing a request
            #Sorting the floor sequence to make the elevator move to the closest target
            self.floor_queue.sort(key=lambda floor:abs(self.floor_heights[floor] - self.mast_cage_joint))
            while self.floor_queue:
                if self.processing_floor_request:
                    break
                self.processing_floor_request = True
                requested_floor = self.floor_queue.pop(0)  # Get the next floor request
                threading.Thread(target=self.move_to_floor, args=(requested_floor,)).start()

        self.processing_timer = None

    
#elevator movement
    def move_to_floor(self, floor):
        
        self.target_height = self.floor_heights.get(floor, 0.0)
        self.get_logger().info(f'Moving to Floor {floor}, Target Height: {self.target_height}')

        # Wait until the target height is reached
        while rclpy.ok() and not self.is_target_reached(self.target_height):
            rclpy.spin_once(self, timeout_sec=0.1)

        self.get_logger().info(f'Arrived at Floor {floor}')
        time.sleep(2)  # Wait for 2 seconds
        self.processing_floor_request = False
        self.process_requests()  # Proceed to next request if any

#checking if the target is reached
    def is_target_reached(self, target_height):
        return abs(self.mast_cage_joint - target_height) < 0.01
    


#elevator acceleration and deceleration on floor requests
    def update_joint_state(self):
    # Calculate the distance to the target height
        self.distance_to_target = abs(self.target_height - self.mast_cage_joint)
        
        # Determine if we need to accelerate or decelerate
        if self.mast_cage_joint < self.target_height:
            direction = 1
        else:
            direction = -1
        
        # Adjust acceleration based on direction
        acceleration_effect = self.acceleration * direction * self.update_period
        
        # Start deceleration earlier by introducing a safety factor 
        safety_factor = 1.5
        stopping_distance = (self.current_speed ** 2) / (2 * abs(self.deceleration)) * safety_factor
        
        # Update speed if not moving or not at max speed yet
        if not self.moving or abs(self.current_speed) < self.max_speed:
            self.current_speed += acceleration_effect
            # Ensure the speed does not exceed max speed
            self.current_speed = min(self.current_speed, self.max_speed) if direction == 1 else max(self.current_speed, -self.max_speed)
        
        # Apply deceleration if within the stopping distance
        if self.distance_to_target < stopping_distance:
            self.current_speed -= self.deceleration * direction * self.update_period
        
        # Update the position based on the current speed
        self.mast_cage_joint += self.current_speed * self.update_period
        
        # Stop moving if close enough to the target
        if self.distance_to_target < 0.01:
            self.moving = False
            self.current_speed = 0  # Ensure the elevator is stopped
        
        # Updating joint positions for ROS2 publishing
        self.joint.header.stamp = self.get_clock().now().to_msg()
        self.joint.position = [self.mast_cage_joint]
        self.joint_state_publisher.publish(self.joint)




def main(args=None):
    rclpy.init(args=args)
    node = StatePublisher()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
