import tkinter as tk   # importing the required library.
import rclpy
from rclpy.node import Node
from std_msgs.msg import Int32


class ElevatorButtonPanel(tk.Frame,Node):
    def __init__(self, master=None, num_floors=10):
        tk.Frame.__init__(self,master)
        Node.__init__(self,'elevator_button_panel')
        self.num_floors = num_floors
        self.master = master
        self.create_widgets()
        self.publisher = self.create_publisher(Int32,'floor_requests',10) #Creating a publisher named floor_requests for state_publisher script
       
      
       
    def create_widgets(self):
        self.selected_floors = []  # Initialize list to store selected floors
        self.buttons = []  # Keep track of buttons for visual feedback

    # Create and arrange floor buttons in a 3x3 grid
        for floor in range(1, 10):  # For 9 floors
            button = tk.Button(self, text=f'Floor {floor}', command=lambda f=floor: self.prepare_floor_request(f))
            button.grid(row=(floor-1)//3, column=(floor-1)%3, padx=5, pady=5)
            self.buttons.append((floor, button))

    # Adding a submit button below the 3x3 grid of floor buttons
        self.submit_button = tk.Button(self, text='Submit', command=self.submit_floor_request)
        self.submit_button.grid(row=3, column=1, pady=10)  # Centering the submit button below the grid

    def prepare_floor_request(self, floor):
        if floor in self.selected_floors:
            self.selected_floors.remove(floor)  # Deselect the floor
        else:
            self.selected_floors.append(floor)  # Select the floor
        print(f'Selected Floors: {self.selected_floors}')

    def submit_floor_request(self):
        for floor in self.selected_floors:
            msg = Int32()
            msg.data = floor
            self.publisher.publish(msg)
            print(f'Publishing floor request: {floor}')
        self.selected_floors.clear()  # Clear the selection after submission


def main():
    rclpy.init()
    root = tk.Tk()
    root.title("Elevator Control Panel")
    app = ElevatorButtonPanel(master=root)
    app.pack(side='top', fill='both',expand=True)
    root.mainloop()
    rclpy.shutdown()

      

  
if __name__ == "__main__":
    main()
