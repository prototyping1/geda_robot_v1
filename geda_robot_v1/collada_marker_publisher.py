import rclpy     # import ROS2 library for Python.
from rclpy.node import Node  # import the necessary module.
from visualization_msgs.msg import Marker #import the marker message type from ROS package.

class MarkerPublisher(Node):        # Defining a class that inherits from 'Node'. This class creates a ROS node for publishing 'Marker' message.
    def __init__(self):
        super().__init__('marker_publisher_node')
    
        self.marker_publisher = self.create_publisher(Marker, 'marker_topic', 10) # creating a publisher for 'Marker' message on the topic 'marker_topic'

        self.timer = self.create_timer(1.0, self.marker_callback) # In this line creates a timer that trigger the 'marker_callback' method every 1 second.

    def marker_callback(self):      # This method creates a 'Marker' message object and sets its properties.
        marker = Marker()
        marker.header.frame_id="world"
        marker.id=0
        marker.type=Marker.MESH_RESOURCE            # Here is defined the marker as a mesh.
        marker.mesh_resource= "package://geda_robot_v1/meshes/building.dae"  # Specifies the mesh location.
        marker.mesh_use_embedded_materials = True
        
        # Defining position, orientation and scale of the marker to be displayes in Rviz.
        marker.pose.position.x = -3.5
        marker.pose.position.y = -3.0
        marker.pose.position.z = 0.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0

        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0

        #Publishing and logging a message indicating that the marker has been published.
        self.marker_publisher.publish(marker)
        self.get_logger().info('Marker Published')
        


def main(args=None):
    rclpy.init(args=args)              # Initializes the ROS system.
    marker_publisher_object = MarkerPublisher() # Creates an instance of the 'MarkerPublisher' class.
    rclpy.spin(marker_publisher_object)        #Enters the ROS event loop, allowing the node to responde to events and callbacks.
    marker_publisher_object.destroy_node()
    rclpy.shutdown()

if __name__== '__main__':
    main()