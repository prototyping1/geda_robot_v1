import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')  

    # URDF file name and its path
    urdf_file_name = 'urdf.urdf'
    geda_robot_package_share_directory = get_package_share_directory('geda_robot_v1')
    urdf = os.path.join(geda_robot_package_share_directory, urdf_file_name)
    with open(urdf, 'r') as infp:           #Reading URDF content.
        robot_desc = infp.read()
    #Rviz configuration file and its path
    rviz_config_file = 'config.rviz'
    rviz_config_path = os.path.join(geda_robot_package_share_directory,rviz_config_file)

    
    return LaunchDescription([          # Defining the launsh description.
        DeclareLaunchArgument(         
            'use_sim_time',
            default_value='false',
            description=''),
        #This node launch the robot state publisher,publishing the state of the robot based on the URDF
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time, 'robot_description': robot_desc}],
            arguments=[urdf]),
        #This launch the state publisher node, publishing the information according to what is indicated in the code.
        Node(
            package='geda_robot_v1',
            executable='state_publisher',
            name='state_publisher',
            output='screen'),
        # This launch the node buttonpanel node.
        Node(
            package='geda_robot_v1',
            executable='buttonpanel',
            name='buttonpanel',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time}]),
        # Launch the node that publish markers for visualization.
        Node(
            package='geda_robot_v1',
            executable='geda_marker_publisher',
            name='marker_publisher_node',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time}]),
        #Launches the Rviz visualization tool with the specified configuration file.
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            output='screen',
            arguments=['-d',rviz_config_path]),

    ])
    
