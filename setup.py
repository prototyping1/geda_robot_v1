import os
from glob import glob
from setuptools import setup
from setuptools import find_packages

package_name = 'geda_robot_v1'

setup(
    name=package_name,
    version='0.0.1',
    packages=[package_name],
    data_files=[                                    # Here is specified the files to be included in the package.
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (os.path.join('share', package_name), glob('urdf/*')),
        (os.path.join('share', package_name,'meshes'),glob('meshes/*')),
        (os.path.join('share', package_name),glob('rviz/config.rviz')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ubuntu',
    maintainer_email='ubuntu@todo.todo',
    description='lifting platform',
    license='RWTH Aachen University',
    tests_require=['pytest'],
    entry_points={                          
        'console_scripts': [
            'state_publisher = geda_robot_v1.state_publisher:main',
            'buttonpanel = geda_robot_v1.buttonpanel:main',
            'geda_marker_publisher = geda_robot_v1.collada_marker_publisher:main',
            
        ],
    },
)
